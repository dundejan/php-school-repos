<?php declare(strict_types=1);

namespace Star;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'star')]
class StarCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setDescription('Draw star')
            ->setHelp('Draw star according to parameters and save it to ".png" file')
            ->addArgument('width', InputArgument::REQUIRED, 'height and width of the image')
            ->addArgument('color', InputArgument::REQUIRED, 'star color')
            ->addArgument('points', InputArgument::REQUIRED, 'number of star points')
            ->addArgument('radius', InputArgument::REQUIRED, 'value from 0 to 1 that defines the thickness of the star points')
            ->addArgument('output', InputArgument::REQUIRED, 'name of the file to save the image to')
            ->addArgument('bgColor', InputArgument::OPTIONAL, 'background color, default is white', 16777215)
            ->addArgument('borderColor', InputArgument::OPTIONAL, 'border color, if not specified, the border will not be rendered', 16777215)
            ->addArgument('borderWidth', InputArgument::OPTIONAL, 'border width in px, if not specified the border will not be rendered', 0)
        ;
    }

    protected function getStarCoords($width, $points, $radius, $size): array
    {
        $coords = array();
        $t = 0;
        for($a = 0;$a <= 360;$a += 360/($points*2))
        {
            $t++;
            if($t % 2 == 0) {
                $coords[] = $width / 2 + ($size * $radius) * sin(deg2rad($a));
                $coords[] = $width / 2 + ($size * $radius) * cos(deg2rad($a));
            }
            else {
                $coords[] = $width/2 + $size * sin(deg2rad($a));
                $coords[] = $width/2 + $size * cos(deg2rad($a));
            }
        }
        return $coords;
    }

    protected function drawFilledStar($img, $width, $sides, $color, $radius, $size = NULL): bool
    {
        $coords = $this->getStarCoords($width, $sides, $radius, $size);
        return imagefilledpolygon($img,$coords,$sides * 2,$color);
    }

    protected function rgbIntToRgbArr($rgbInt): array
    {
        $blue =  $rgbInt % 256;
        $green = (int)($rgbInt / 256) % 256;
        $red =   (int) ($rgbInt / 256 / 256) % 256;

        return array($red, $green, $blue);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $width = $input->getArgument('width');
        $im = imagecreate((int) $width, (int) $width);
        imagecolorallocate($im, ...$this->rgbIntToRgbArr($input->getArgument('bgColor')));

        $color = imagecolorallocate($im, ...$this->rgbIntToRgbArr($input->getArgument('color')));
        $points = $input->getArgument('points');
        $radius = $input->getArgument('radius');
        $file = $input->getArgument('output');

        $borderColor = imagecolorallocate($im, ...$this->rgbIntToRgbArr($input->getArgument('borderColor')));
        $borderWidth = $input->getArgument('borderWidth');

        //DRAW FULL-SIZE STAR TO CREATE BORDER
        $this->drawFilledStar($im, $width, $points, $borderColor, $radius, $width/2);

        //DRAW STAR SMALLER BY BORDER-WIDTH TO CREATE FILLING OF THE STAR
        $this->drawFilledStar($im, $width, $points, $color, $radius, $width/2 - $borderWidth);

        $final = imagerotate($im, 180, $borderColor);

        //SAVE TO OUTPUT FILE
        if (pathinfo($file)['extension'] == 'png') {
            imagepng($final, $file);
            $output->write("Star was successfully created and saved to '" . $file . "'.");
        }
        else if (pathinfo($file)['extension'] == '') {
            imagepng($final, $file . '.png');
            $output->write("Star was successfully created and saved to '" . $file . ".png'.");
        }
        else {
            $output->write("Star was not created. Wrong file name. It must be specified without extension or with '.png' extension.");
        }

        return command::SUCCESS;
    }
}
