<?php declare(strict_types=1);

function getPrice(string $item): float {
    $item = preg_replace('/(( )?Kč|,-)/', 'CZK', $item);
    $item = preg_replace('/(\d+)?(\.)?(\d+)(\.)?(\d*)(,)?(\d*)( )?CZK/', 'CZK$1$2$3$4$5$6$7$8', $item);
    $item = preg_replace('/CZK /', 'CZK', $item);
    $item = preg_replace('/\./', '', $item);
    $item = preg_replace('/,/', '.', $item);

    //echo $item;
    //echo PHP_EOL;

    $len = strlen($item);
    $numstr = '';
    for ($index = 0; $index < $len; $index++) {
        if ($item[$index] == 'C' && ($index+1) < $len && $item[$index+1] == 'Z' && ($index+2) < $len && $item[$index+2] == 'K') {
            $index += 3;
            while ($index < $len && (is_numeric($item[$index]) || $item[$index] == '.')) {
                $numstr .= $item[$index];
                $index++;
            }
            break;
        }
    }
    $price = floatval($numstr);
    //echo $price;
    //echo PHP_EOL;

    return $price;
}

assert(getPrice("Máslo 49,05 Kč") == 49.05);
assert(getPrice("CZK400 Knížka") == 400);
assert(getPrice("Pivo 42,-") == 42);
assert(getPrice("Máslo 49,00 Kč") == 49.00);
assert(getPrice("CZK 1.600,59 Natural 95") == 1600.59);
assert(getPrice("CZK189 1984") == 189);
assert(getPrice("32bit CPU CZK990,00") == 990);
assert(getPrice("Blade Runner 2049 129,-") == 129);
assert(getPrice("iPhone 14 40.990Kč") == 40990);
assert(getPrice("BMW 525 2.590.999CZK") == 2590999);

/**
 * @param string[] $list
 * @return string[]
 */
function sortList(array $list): array {
    usort($list, function ($a, $b) {
        return (getPrice($a) < getPrice($b)) ? -1 : 1;
    });
    return $list;
}

/**
 * @param string[] $list
 */
function sumList(array $list): float {
    $sum = 0.0;
    foreach ($list as $item) {
        $sum += getPrice($item);
    }

    return $sum;
}

if (count($argv) !== 2) {
	echo "Usage: php shopping.php <input>" . PHP_EOL;
	exit(1);
}

$input = trim(file_get_contents(end($argv)));
$list = explode(PHP_EOL, $input);
$list = sortList($list);
print_r($list);
print_r(sumList($list) . PHP_EOL);