<?php declare(strict_types=1);

namespace HW\Tests;

use HW\Lib\Storage;
use HW\Lib\UserService;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    protected UserService $userService;
    protected Storage $storage;

    public function setUp(): void
    {
        $this->storage = $this->getMockBuilder(Storage::class)->getMock();
        $this->userService = new UserService($this->storage);
    }

    /**
     * @dataProvider generateRandomUser
     */
    public function testUserService ($username, $email) {
        self::assertEquals(NULL, $this->userService->getUsername(uniqid('', true)));
        self::assertEquals(NULL, $this->userService->getEmail(uniqid('', true)));

        $this->storage->expects($this->once())->method('save');
        $id = $this->userService->createUser($username, $email);

        $userArray = array('username' => $username, 'email' => $email);
        $this->storage->expects($this->any())
            ->method('get')
            ->with($id)
            ->will($this->returnValue(json_encode($userArray)));
        self::assertEquals($username, $this->userService->getUsername($id));
        self::assertEquals($email, $this->userService->getEmail($id));
    }

    public function generateRandomUser() : \Generator {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        $NUM_OF_GENERATED = 10;
        for ($i = 0; $i < $NUM_OF_GENERATED; ++$i) {
            $username = '';
            $email = '';
            $length = rand(5, 50);
            for ($i = 0; $i < $length; ++$i) {
                $username .= $characters[rand(0, $charactersLength - 1)];
                $email .= $characters[rand(0, $charactersLength - 1)];
            }
            $length = rand(1, 10);
            for ($i = 0; $i < $length; $i++) {
                $email .= $characters[rand(0, $charactersLength - 1)];
            }
            $email .= '@';
            $length = rand(1, 10);
            for ($i = 0; $i < $length; $i++) {
                $email .= $characters[rand(0, $charactersLength - 1)];
            }
            $email .= ".com";

            yield [$username, $email];
        }
    }
}
