<?php declare(strict_types=1);

namespace HW\Tests;

use HW\Lib\MathUtils;
use PHPUnit\Framework\TestCase;

class MathUtilsTest extends TestCase
{
    public function testSum () {
        self::assertEquals(3, MathUtils::sum([1,2]));
        self::assertEquals(-123456789, MathUtils::sum([1,2,-1,-2,-123456789]));
        self::assertEquals(0.0, MathUtils::sum([1,2,-1,-2,0,-0,-0.000,1.0001,-1.0001]));
    }

    public function testSolveLinear () {
        self::assertEquals(0, MathUtils::solveLinear(rand(1, PHP_INT_MAX), 0));
        self::assertEquals(10, MathUtils::solveLinear(2, -20));
        self::assertEquals(50000000, MathUtils::solveLinear(0.0000001, -5));
        self::expectException(\InvalidArgumentException::class);
        MathUtils::solveLinear(0, rand());
    }

    public function testSolveQuadratic () {
        self::assertEqualsCanonicalizing([-1,0], MathUtils::solveQuadratic(2,2,0));
        self::assertEqualsCanonicalizing([-1], MathUtils::solveQuadratic(1,2,1));
        self::assertEqualsCanonicalizing([1], MathUtils::solveQuadratic(1,-2,1));
    }
}
