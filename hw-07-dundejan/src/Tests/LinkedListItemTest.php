<?php declare(strict_types=1);

namespace HW\Tests;

use HW\Lib\LinkedList;
use HW\Lib\LinkedListItem;
use PHPUnit\Framework\TestCase;

class LinkedListItemTest extends TestCase
{
    protected LinkedList $list;

    /**
     * @var int Can be modified, but needs to be set to at least 10.
     */
    private int $TEST_SIZE = 30;

    public function setUp(): void
    {
        parent::setUp();
        $this->list = new LinkedList();
    }

    /**
     * @dataProvider valueGenerator
     */
    public function testAppendList ($values) {
        for ($idx = 0; $idx < $this->TEST_SIZE; ++$idx) {
            $item = $this->list->appendList($values[$idx]);
            self::assertEquals($values[$idx], $item->getValue());
        }

        $pos = $this->TEST_SIZE-1;
        $values[$pos] = "newName";
        $item->setValue($values[$pos]);
        self::assertEquals($values[$pos],$item->getValue());

        $item = $this->list->getFirst();
        for ($idx = 0; $idx < $this->TEST_SIZE; ++$idx) {
            self::assertEquals($values[$idx], $item->getValue());
            $item = $item->getNext();
        }

        $item = $this->list->getLast();
        for ($idx = $this->TEST_SIZE-1; $idx >= 0; --$idx) {
            self::assertEquals($values[$idx], $item->getValue());
            $item = $item->getPrev();
        }
    }

    /**
     * @depends testAppendList
     * @dataProvider valueGenerator
     */
    public function testAppendItem ($values) {
        $this->list->appendList($values[0]);

        $this->list->appendItem($this->list->getLast(), $values[1]);
        self::assertEquals($values[1], $this->list->getLast()->getValue());

        $this->list->appendItem($this->list->getLast(), $values[3]);
        self::assertEquals($values[3], $this->list->getLast()->getValue());

        $this->list->appendItem($this->list->getLast()->getPrev(), $values[2]);
        self::assertEquals($values[2], $this->list->getFirst()->getNext()->getNext()->getValue());

        $this->list->appendItem($this->list->getLast(), $values[4]);
        self::assertEquals($values[4], $this->list->getLast()->getValue());
    }

    /**
     * @dataProvider valueGenerator
     */
    public function testPrependList ($values) {
        for ($idx = $this->TEST_SIZE-1; $idx >= 0; --$idx) {
            $item = $this->list->prependList($values[$idx]);
            self::assertEquals($values[$idx], $item->getValue());
        }

        $values[0] = "newName";
        $item->setValue($values[0]);
        self::assertEquals($values[0],$item->getValue());

        $item = $this->list->getFirst();
        for ($idx = 0; $idx < $this->TEST_SIZE; ++$idx) {
            self::assertEquals($values[$idx], $item->getValue());
            $item = $item->getNext();
        }

        $item = $this->list->getLast();
        for ($idx = $this->TEST_SIZE-1; $idx >= 0; --$idx) {
            self::assertEquals($values[$idx], $item->getValue());
            $item = $item->getPrev();
        }
    }

    /**
     * @depends testAppendList
     * @dataProvider valueGenerator
     */
    public function testPrependItem ($values) {
        $this->list->appendList($values[5]);

        $this->list->prependItem($this->list->getLast(), $values[4]);
        self::assertEquals($values[4], $this->list->getLast()->getPrev()->getValue());
        self::assertEquals($values[4], $this->list->getFirst()->getValue());

        $this->list->prependItem($this->list->getFirst(), $values[2]);
        self::assertEquals($values[2], $this->list->getFirst()->getValue());

        $this->list->prependItem($this->list->getFirst()->getNext(), $values[3]);
        self::assertEquals($values[3], $this->list->getLast()->getPrev()->getPrev()->getPrev()->getValue());

        $this->list->prependItem($this->list->getFirst(), $values[1]);
        self::assertEquals($values[1], $this->list->getFirst()->getValue());

        $this->list->prependItem($this->list->getFirst(), $values[0]);
        self::assertEquals($values[0], $this->list->getFirst()->getValue());
    }

    public function valueGenerator () : \Generator
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $values = array();

        for ($idx = 0; $idx < $this->TEST_SIZE; ++$idx) {
            $values[$idx] = '';
            $length = rand(5, 20);
            for ($i = 0; $i < $length; ++$i) {
                $values[$idx] .= $characters[rand(0, $charactersLength - 1)];
            }
        }
        yield([$values]);
    }
}
