<?php declare(strict_types=1);

namespace App\Model;

use App\Db;
use mysql_xdevapi\Exception;
use PDO;

class Account
{
    public function __construct(
        protected int    $id,
        protected string $number,
        protected string $code
    )
    {
    }

    /**
     * Creates DB table using CREATE TABLE ...
     */
    public static function createTable(): void
    {
        $db = Db::get();
        $db->query('CREATE TABLE IF NOT EXISTS `account` (
            id       INTEGER not null
            constraint account_pk
            primary key autoincrement,
            number   TEXT not null,
            code     TEXT not null
        )');
    }

    /**
     * Drops DB table using DROP TABLE ...
     */
    public static function dropTable(): void
    {
        $db = Db::get();
        $db->query('DROP TABLE IF EXISTS `account`');
    }

    /**
     * Find account record by number and bank code
     */
    public static function find(string $number, string $code): ?self
    {
        $db = Db::get();
        $query= 'SELECT id, number, code FROM `account` WHERE 
                            number = :number AND 
                            code = :code';
        $statement = $db->prepare($query);
        $statement->execute([
            'number' => $number,
            'code' => $code
        ]);
        $result = $statement->fetch();
        if (!$result) {
            return NULL;
        }

        return new Account ($result[0], $number, $code);
    }

    /**
     * Find account record by id
     */
    public static function findById(int $id): ?self
    {
        $db = Db::get();
        $query= 'SELECT * FROM `account` WHERE id = :id';
        $statement = $db->prepare($query);
        $statement->execute([
            'id' => $id
        ]);
        $result = $statement->fetch();
        if (!$result) {
            return NULL;
        }

        return new Account($result[0], (string)$result["number"], (string)$result["code"]);
    }

    /**
     * Inserts new account record and returns its instance; or returns existing account instance
     */
    public static function findOrCreate(string $number, string $code): self
    {
        $find = self::find($number, $code);
        if ($find != NULL)
            return $find;

        $db = Db::get();
        $query = "INSERT INTO `account` (number, code) VALUES (:number, :code)";
        $statement = $db->prepare($query);
        $statement->execute([
            'number' => $number,
            'code' => $code
        ]);
        $id = $db->lastInsertId();

        return new Account((int)$id, $number, $code);
    }

    /**
     * Returns iterable of Transaction instances related to this Account, consider both transaction direction
     *
     * @return iterable<Transaction>
     */
    public function getTransactions(): iterable
    {
        $db = Db::get();
        $query = "SELECT * FROM `transaction` WHERE \"from\" = :idAcc OR \"to\" = :idAcc";
        $statement = $db->prepare($query);
        $statement->execute([
            'idAcc' => $this->id
        ]);
        $resultArr = $statement->fetchAll();
        if (!$resultArr) {
            return [];
        }

        $resultObjects = array();

        foreach ($resultArr as $resultItem) {
            $resultObjects[] = new Transaction(self::findById($resultItem[1]), self::findById($resultItem[2]), $resultItem[3]);
        }

        return $resultObjects;
    }

    /**
     * Returns transaction sum (using SQL aggregate function). Treat outgoing transactions as 'minus' and incoming as 'plus'.
     */
    public function getTransactionSum(): float
    {
        $db = Db::get();
        $query = "SELECT SUM(amount) FROM `transaction` WHERE \"to\" = :idAcc";
        $statement = $db->prepare($query);
        $statement->execute([
            'idAcc' => $this->id
        ]);
        $to = $statement->fetch()[0];
        $query = "SELECT SUM(amount) FROM `transaction` WHERE \"from\" = :idAcc";
        $statement = $db->prepare($query);
        $statement->execute([
            'idAcc' => $this->id
        ]);
        $from = $statement->fetch()[0];

        return $to - $from;
    }

    public static function printAllAccounts(): void
    {
        $db = Db::get();
        $query= 'SELECT * FROM `account`';
        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $resItem) {
            print_r($resItem);
            print(PHP_EOL);
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Account
    {
        $this->id = $id;
        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): Account
    {
        $this->number = $number;
        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): Account
    {
        $this->code = $code;
        return $this;
    }

    public function __toString(): string
    {
        return "{$this->number}/{$this->code}";
    }
}
