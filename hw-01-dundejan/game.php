<?php declare(strict_types=1);

const DEAD = '.';
const ALIVE = 'X';

$matrix = array();

function readInput($string): array
{
    global $matrix;
    $matrix = explode(PHP_EOL, $string);

    //print_r($matrix);

    return $matrix;
}

function writeOutput($matrix): string
{
    $string = implode(PHP_EOL, $matrix);

    //print($string);

    return $string;
}


//SOME FAST TESTING:
/*  $fileContent = file_get_contents('input/glider.txt');
    readInput($fileContent);
    print(writeOutput($matrix)); print(PHP_EOL); print(PHP_EOL);
    $matrix = gameStep($matrix);
    print(writeOutput($matrix)); print(PHP_EOL); print(PHP_EOL);
    $matrix = gameStep($matrix);
*/

function gameStep($matrix) {
    $newMatrix = $matrix;
    $i = 0;

    $rows = count($matrix);
    $columns = strlen($matrix[0]);

    while ($i < $rows) {
        $j = 0;
        while ($j < $columns) {
            if ($matrix[$i][$j] == ALIVE) {
                $living_neighbours = getNeighbours($i, $j, $matrix, $rows, $columns);

                if ($living_neighbours < 2 || $living_neighbours > 3)
                    $newMatrix[$i][$j] = DEAD;
                //else zustava stale ziva, takze nedelam nic
            } else {
                $living_neighbours = getNeighbours($i, $j, $matrix, $rows, $columns);

                if ($living_neighbours == 3)
                    $newMatrix[$i][$j] = ALIVE;
                //else zustava stale mrtva, takze nedelam nic
            }
            $j++;
        }
        $i++;
    }
    return $newMatrix;
}

function getNeighbours(int $i, int $j, $matrix, $max_i, $max_j): int
{
    //CORRECTION OF "Uninitialized string offset" AFTER SOME READING ABOUT IT
    $max_i--;
    $max_j--;

    $living_neighbours = 0;

    if ($i > 0 && $j > 0 && $matrix[$i - 1][$j - 1] == ALIVE)
        $living_neighbours++;
    if ($i > 0 && $matrix[$i - 1][$j] == ALIVE)
        $living_neighbours++;
    if ($i > 0 && $j < $max_j && $matrix[$i - 1][$j + 1] == ALIVE)
        $living_neighbours++;

    if ($j > 0 && $matrix[$i][$j - 1] == ALIVE)
        $living_neighbours++;
    if ($j < $max_j && $matrix[$i][$j + 1] == ALIVE)
        $living_neighbours++;

    if ($i < $max_i && $j > 0 && $matrix[$i + 1][$j - 1] == ALIVE)
        $living_neighbours++;
    if ($i < $max_i && $matrix[$i + 1][$j] == ALIVE)
        $living_neighbours++;
    if ($i < $max_i && $j < $max_j && $matrix[$i + 1][$j + 1] == ALIVE)
        $living_neighbours++;

    return $living_neighbours;
}
