<?php declare (strict_types=1);

class Bag {
    protected array $content = [];
    public function add(mixed $item): void
    {
        $this->content[] = $item;
    }

    public function clear(): void
    {
        unset($this->content);
        $this->content = array();
    }

    public function contains(mixed $item): bool
    {
        if (($key = array_search($item, $this->content)) !== false) {
            return true;
        }
        else return false;
    }

    public function elementSize(mixed $item): int
    {
        return count(array_keys($this->content, $item));
    }

    public function isEmpty(): bool
    {
        return (empty($this->content));
    }

    public function remove(mixed $item): void
    {
        if (($key = array_search($item, $this->content)) !== false) {
            unset($this->content[$key]);
        }
    }

    public function size(): int
    {
        return count($this->content);
    }
}
