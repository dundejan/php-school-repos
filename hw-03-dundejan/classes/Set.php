<?php

class Set extends Bag
{
    public function add(mixed $item): void
    {
        if (!$this->contains($item))
        $this->content[] = $item;
    }
}