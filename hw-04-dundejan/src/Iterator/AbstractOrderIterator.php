<?php declare(strict_types=1);

namespace Iterator;

use Node;

abstract class AbstractOrderIterator implements \Iterator
{
    protected ?Node $position;
    protected array $treeArray;
    protected int $arrPosition = 0;

    public function __construct(Node $root)
    {
        $this->toArray($root);
        $this->position = $this->treeArray[0];
    }

    public function current(): ?Node
    {
        return $this->position;
    }

    public function next(): void
    {
        if (isset($this->treeArray[++$this->arrPosition])) {
            $this->position = $this->treeArray[$this->arrPosition];
        }
    }

    public function key(): bool|int|float|string|null
    {
        return $this->position->getValue();
    }

    public function valid(): bool
    {
        return isset($this->treeArray[$this->arrPosition]);
    }

    public function rewind(): void
    {
        $this->arrPosition = 0;
    }
}
