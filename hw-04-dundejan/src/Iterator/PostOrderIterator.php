<?php declare(strict_types=1);

namespace Iterator;

use Node;

class PostOrderIterator extends AbstractOrderIterator
{
    public function toArray(Node $node)
    {
        if ($node->getLeft() != null)
            $this->toArray($node->getLeft());
        if ($node->getRight() != null)
            $this->toArray($node->getRight());
        $this->treeArray [] = $node;
    }
}