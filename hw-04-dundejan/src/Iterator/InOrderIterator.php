<?php declare(strict_types=1);

namespace Iterator;

use Node;

class InOrderIterator extends AbstractOrderIterator
{
    public function toArray(Node $node)
    {
        if ($node->getLeft() != null)
            $this->toArray($node->getLeft());
        $this->treeArray [] = $node;
        if ($node->getRight() != null)
            $this->toArray($node->getRight());
    }
}