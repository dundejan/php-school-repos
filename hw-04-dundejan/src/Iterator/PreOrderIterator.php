<?php declare(strict_types=1);

namespace Iterator;

use Node;

class PreOrderIterator extends AbstractOrderIterator
{
    public function toArray(Node $node)
    {
        $this->treeArray [] = $node;
        if ($node->getLeft() != null)
            $this->toArray($node->getLeft());
        if ($node->getRight() != null)
            $this->toArray($node->getRight());
    }
}