<?php declare(strict_types=1);

use Books\Middleware\JsonBodyParserMiddleware;
use Books\Model\Book;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response as Psr7Response;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);
$app->add(new JsonBodyParserMiddleware());

$securityMiddleware = function (Request $request, RequestHandler $handler): Response {
    if (!$request->hasHeader('Authorization')) {
        $response = new Psr7Response();
        return $response->withStatus(401);
    }

    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];

    if ($username != 'admin' || $password != 'pas$word') {
        $response = new Psr7Response();
        return $response->withStatus(401);
    }

    //$request = $request->withAttribute('customer', $customer);
    return $handler->handle($request);
};

$app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write('Funguje to! Ale nic tady není.');
    return $response;
});

$app->get('/books', function (Request $request, Response $response, $args) {
    $payload = json_encode(Book::getAll());
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->get('/books/{id}', function (Request $request, Response $response, $args) {
    $id = $args['id'];
    $book = Book::find((int)$id);
    if ($book == null) {
        return $response->withStatus(404);
    }
    $payload = json_encode($book);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
});

$app->post('/books', function (Request $request, Response $response, $args): Response {
    $data = $request->getParsedBody();
    if (!isset($data['name'])
        || !isset($data['author'])
        || !isset($data['publisher'])
        || !isset($data['isbn'])
        || !isset($data['pages'])) {
        return $response->withStatus(400);
    }
    $book = Book::create($data['name'], $data['author'], $data['publisher'], $data['isbn'], (int)$data['pages']);

    $location = '/books/' . $book->getId();
    return $response->withStatus(201)->withHeader('Location', $location);
})->add($securityMiddleware);

$app->put('/books/{id}', function (Request $request, Response $response, $args): Response {
    $id = $args['id'];
    $book = Book::find((int)$id);
    if ($book == null) {
        return $response->withStatus(404);
    }

    $data = $request->getParsedBody();
    if (!isset($data['name'])
        || !isset($data['author'])
        || !isset($data['publisher'])
        || !isset($data['isbn'])
        || !isset($data['pages'])) {
        return $response->withStatus(400);
    }

    Book::updateById((int)$id, $data['name'], $data['author'], $data['publisher'], $data['isbn'], (int)$data['pages']);
    return $response->withStatus(204);
})->add($securityMiddleware);

$app->delete('/books/{id}', function (Request $request, Response $response, $args): Response {
    $id = $args['id'];
    $book = Book::find((int)$id);
    if ($book == null) {
        return $response->withStatus(404);
    }

    Book::deleteById((int)$id);
    return $response->withStatus(204);
})->add($securityMiddleware);

$app->run();