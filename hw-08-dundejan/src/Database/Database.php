<?php

namespace Books\Database;

use PDO;

class Database
{
    private const DSN = 'sqlite:database.sqlite';

    private static ?PDO $db = null;

    public static function getConnection(): PDO
    {
        if (self::$db === null) {
            self::$db = new PDO(self::DSN);
        }
        return self::$db;
    }
}