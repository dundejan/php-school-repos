<?php

namespace Books\Model;

use Books\Database\Database;
use PDO;

class Book
{
    private int $id;
    private string $name;
    private string $author;
    private string $publisher;
    private string $isbn;
    private int $pages;

    /**
     * @param int $id
     * @param string $name
     * @param string $author
     * @param string $publisher
     * @param string $isbn
     * @param int $pages
     */
    public function __construct(int $id, string $name, string $author, string $publisher, string $isbn, int $pages)
    {
        $this->id = $id;
        $this->name = $name;
        $this->author = $author;
        $this->publisher = $publisher;
        $this->isbn = $isbn;
        $this->pages = $pages;
    }

    /**
     * Creates DB table using CREATE TABLE ...
     */
    public static function createTable(): void
    {
        $db = Database::getConnection();
        $db->query('CREATE TABLE IF NOT EXISTS book (
            id       INTEGER not null
            constraint account_pk
            primary key autoincrement,
            name   TEXT not null,
            author     TEXT not null,
            publisher     TEXT not null,
            isbn     TEXT not null,
            pages     INTEGER not null
        )');
    }

    /**
     * Drops DB table using DROP TABLE ...
     */
    public static function dropTable(): void
    {
        $db = Database::getConnection();
        $db->query('DROP TABLE IF EXISTS `book`');
    }

    /**
     * Create new book record
     * @param $name
     * @param $author
     * @param $publisher
     * @param $isbn
     * @param int $pages
     * @return static
     */
    public static function create($name, $author, $publisher, $isbn, int $pages): self
    {
        $db = Database::getConnection();
        $query = "INSERT INTO `book` (name, author, publisher, isbn, pages) VALUES (:name, :author, :publisher, :isbn, :pages)";
        $statement = $db->prepare($query);
        $statement->execute([
            'name' => $name,
            'author' => $author,
            'publisher' => $publisher,
            'isbn' => $isbn,
            'pages' => $pages
        ]);

        return new Book((int)$db->lastInsertId(), $name, $author, $publisher, $isbn, $pages);
    }

    /**
     * Get all books records from database
     * @return array
     */
    public static function getAll(): array
    {
        $db = Database::getConnection();
        $query= 'SELECT id, name, author FROM `book`';
        $statement = $db->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Find book record by id
     * @param int $id
     * @return mixed|null
     */
    public static function find(int $id)
    {
        $db = Database::getConnection();
        $query= 'SELECT * FROM `book` WHERE id = :id';
        $statement = $db->prepare($query);
        $statement->execute([
            'id' => $id
        ]);
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            return NULL;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Update book record by id
     */
    public static function updateById(int $id, $name, $author, $publisher, $isbn, int $pages): void
    {
        $db = Database::getConnection();
        $query= 'UPDATE `book` SET 
                  name = :name,
                  author = :author,
                  publisher = :publisher,
                  isbn = :isbn,
                  pages = :pages 
              WHERE id = :id';
        $statement = $db->prepare($query);
        $statement->execute([
            'id' => $id,
            'name' => $name,
            'author' => $author,
            'publisher' => $publisher,
            'isbn' => $isbn,
            'pages' => $pages
        ]);
    }

    /**
     * Delete book record by id
     */
    public static function deleteById(int $id): void
    {
        $db = Database::getConnection();
        $query= 'DELETE FROM `book` WHERE id = :id';
        $statement = $db->prepare($query);
        $statement->execute([
            'id' => $id
        ]);
    }
}