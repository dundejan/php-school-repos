<?php declare(strict_types=1);

namespace Dundejan\App;

use Dompdf\Dompdf;

class Renderer extends Dompdf
{
    public function makeInvoice(Invoice $invoice): string
    {
        $this->setPaper('A4');

        $supplierZip = substr($invoice->getSupplier()->getAddress()->getZipCode(), 0, 3) . ' ' .
            substr($invoice->getSupplier()->getAddress()->getZipCode(), 3, 2);
        $supplier = '<b>Dodavatel</b><br><br>' .
            $invoice->getSupplier()->getName() . '<br>' .
            $invoice->getSupplier()->getAddress()->getStreet() . ' ' .
            $invoice->getSupplier()->getAddress()->getNumber() . '<br>' .
            $supplierZip . ' ' .
            $invoice->getSupplier()->getAddress()->getCity() . '<br><br>' .
            $invoice->getSupplier()->getVatNumber() . '<br><br>' .
            $invoice->getSupplier()->getAddress()->getPhone() . '<br>' .
            $invoice->getSupplier()->getAddress()->getEmail() . '<br>';

        $customerZip = substr($invoice->getCustomer()->getAddress()->getZipCode(), 0, 3) . ' ' .
                substr($invoice->getCustomer()->getAddress()->getZipCode(), 3, 2);
            $customer = '<b>Odberatel</b><br><br>' .
            $invoice->getCustomer()->getName() . '<br>' .
            $invoice->getCustomer()->getAddress()->getStreet() . ' ' .
            $invoice->getCustomer()->getAddress()->getNumber() . '<br>' .
            $customerZip . ' ' .
            $invoice->getCustomer()->getAddress()->getCity() . '<br><br>' .
            $invoice->getCustomer()->getVatNumber() . '<br><br>' .
            $invoice->getCustomer()->getAddress()->getPhone() . '<br>' .
            $invoice->getCustomer()->getAddress()->getEmail() . '<br>';

        $items = '';
        $totalPrice = 0.0;
        foreach ($invoice->getItems() as $item) {
            $items .= '<tr style="height:15pt"><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s2" style="padding-left: 5pt;text-indent: 0;line-height: 14pt;text-align: left;">
                ' . $item->getDescription() . '
</p></td><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s2" style="padding-right: 4pt;text-indent: 0;line-height: 14pt;text-align: right;">
                ' . $item->getQuantity() . '
</p></td><td style="width:114pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s2" style="padding-right: 4pt;text-indent: 0;line-height: 14pt;text-align: right;">
                ' . number_format($item->getUnitPrice(), 2, ',', ' ') . '
            </p></td><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s2" style="padding-right: 4pt;text-indent: 0;line-height: 14pt;text-align: right;">
                ' . number_format($item->getTotalPrice(), 2, ',', ' ') . '
            </p></td></tr>';
            $totalPrice += $item->getTotalPrice();
        }

        $html = '
<html lang="cs"><head><style>
 p { font-family:Calibri, sans-serif; margin:0; font-size:15px;}
 .s1 { font-family:Calibri, sans-serif; }
 .s2 { font-family:Calibri, sans-serif; }
 .s3 { font-family:Calibri, sans-serif; }
 table, tbody
</style><title>FAKTURA</title></head><body>
<p style="padding-top: 30pt;padding-left: 33pt;text-indent: 0;text-align: left;">
FAKTURA – DOKLAD c. ' . $invoice->getNumber() . '
</p>
<p style="text-indent: 0;text-align: left;"><br/></p><table style="border-collapse:collapse;margin-left:auto;margin-right:auto"><tr style="height:22pt"><td style="width:226pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-align: left;">
' . $supplier . '
</p>
</td><td style="width:227pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0;text-align: left;">
' . $customer . '
</p>
</td><tr style="height:37pt"><td style="width:226pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s2" style="padding-top: 7pt;padding-left: 5pt;text-indent: 0;text-align: left;">
</a></p></td><td style="width:227pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s2" style="padding-top: 7pt;padding-left: 5pt;text-indent: 0;text-align: left;">
</p></td></tr></table><p style="text-indent: 0;text-align: left;"><br/></p><table style="border-collapse:collapse;margin-left:auto;margin-right:auto" cellspacing="0"><tr style="height:15pt"><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0;line-height: 14pt;text-align: left;">
<b>Polozka</b>
</p></td><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0;line-height: 14pt;text-align: left;">
<b>Pocet m.j.</b>
</p></td><td style="width:114pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0;line-height: 14pt;text-align: left;">
<b>Cena za m.j.</b>
</p></td><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-left: 5pt;text-indent: 0;line-height: 14pt;text-align: left;">
<b>Celkem</b>
</p></td></tr>
' . $items . '
<tr style="height:15pt"><td style="width:340pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt" colspan="3"><p class="s1" style="padding-left: 5pt;text-indent: 0;line-height: 14pt;text-align: left;">
<b>Celkem</b>
</p></td><td style="width:113pt;border-top-style:solid;border-top-width:1pt;border-left-style:solid;border-left-width:1pt;border-bottom-style:solid;border-bottom-width:1pt;border-right-style:solid;border-right-width:1pt"><p class="s1" style="padding-right: 4pt;text-indent: 0;line-height: 14pt;text-align: right;">
<b>' . number_format($totalPrice, 2, ',', ' ') . '</b>
</p></td></tr></table></body></html>
 ';

        $this->loadHtml($html);
        $this->render();
        return $this->output();
    }
}