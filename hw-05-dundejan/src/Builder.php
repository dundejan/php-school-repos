<?php declare(strict_types=1);

namespace Dundejan\App;

class Builder
{
    protected Invoice $invoice;
    public function __construct()
    {
        $this->invoice = new Invoice();
    }

    public function build(): Invoice
    {
        return $this->invoice;
    }

    public function setNumber(string $number): self
    {
        // TODO implement
        $this->invoice->setNumber($number);
        return $this;
    }

    public function setSupplier(
        string  $name,
        string  $vatNumber,
        string  $street,
        string  $number,
        string  $city,
        string  $zip,
        ?string $phone = null,
        ?string $email = null
    ): self
    {
        $supplier = new Invoice\BusinessEntity();
        $address = new Invoice\Address();
        $address->setStreet($street)
            ->setNumber($number)
            ->setCity($city)
            ->setZipCode($zip)
            ->setPhone($phone)
            ->setEmail($email);
        $supplier->setName($name)
            ->setVatNumber($vatNumber)
            ->setAddress($address);
        $this->invoice->setSupplier($supplier);
        return $this;
    }

    public function setCustomer(
        string  $name,
        string  $vatNumber,
        string  $street,
        string  $number,
        string  $city,
        string  $zip,
        ?string $phone = null,
        ?string $email = null
    ): self
    {
        $customer = new Invoice\BusinessEntity();
        $address = new Invoice\Address();
        $address->setStreet($street)
            ->setNumber($number)
            ->setCity($city)
            ->setZipCode($zip)
            ->setPhone($phone)
            ->setEmail($email);
        $customer->setName($name)
            ->setVatNumber($vatNumber)
            ->setAddress($address);
        $this->invoice->setCustomer($customer);
        return $this;
    }

    public function addItem(string $description, ?float $quantity, ?float $price): self
    {
        // TODO implement
        $item = new Invoice\Item();
        $item->setDescription($description)->setQuantity($quantity)->setUnitPrice($price);
        $this->invoice->addItem($item);
        return $this;
    }
}
